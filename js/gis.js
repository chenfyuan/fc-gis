(function(document, action, map) {
	var layerArray = map.getLayers().getArray();
	console.log(layerArray);
	action.showOrHideLayer = function() {
			var pointButton = document.getElementById("point_layer");
			var layerId = pointButton.getAttribute("layerId");
			var selectedLayer = layerArray[layerId];
			map.removeLayer(selectedLayer);
		},
		action.showOrHideLine = function() {
			var lineButton = document.getElementById("line_layer");
			var layerId = lineButton.getAttribute("layerId");
			var selectedLayer = layerArray[layerId];
			map.removeLayer(selectedLayer);
		},
		action.showOrHideFace = function() {
			var faceButton = document.getElementById("face_layer");
			var layerId = faceButton.getAttribute("layerId");
			var selectedLayer = layerArray[layerId];
			map.removeLayer(selectedLayer);
		}

})(document, window.action = {}, map);
//初始化地图

	var pos = ol.proj.fromLonLat([108.332, 21.612]);
	var mapView = new ol.View({
		center: pos, // 定义地图显示中心于经度0度，纬度0度处
		projection: 'EPSG:3857',
		zoom: 16, // 缩放级别，数字越大缩放级别越高
		minZoom: 1, //约束缩放范围
		maxZoom: 30,

	});
	  var  zoomslider=new ol.control.ZoomSlider();
 
	var layer1 = new ol.layer.Tile({
		title: "天地图路网",
		source: new ol.source.XYZ({
			url: "http://t4.tianditu.com/DataServer?T=vec_w&x={x}&y={y}&l={z}"
		})
	});
	var layer2 = new ol.layer.Tile({
		title: "天地图文字标注",
		source: new ol.source.XYZ({
			url: 'http://t3.tianditu.com/DataServer?T=cva_w&x={x}&y={y}&l={z}'
		})

	});
	var fcgImage = new ol.layer.Image({
		title: '防城港多边形底图',
		source: new ol.source.ImageWMS({
			ratio: 1,
			url: 'http://120.241.64.28:9770/geoserver/cite/wms',
			params: {
				'FORMAT': 'image/png',
				'VERSION': '1.1.1',
				LAYERS: 'cite:polygon_new',
				STYLES: ''
			}
		})
	});
	var fcgImage1 = new ol.layer.Image({
		title: '防城港折现底图',
		source: new ol.source.ImageWMS({
			ratio: 1,
			url: 'http://120.241.64.28:9770/geoserver/cite/wms',
			params: {
				'FORMAT': 'image/png',
				'VERSION': '1.1.1',
				LAYERS: 'cite:polyline_new',
				STYLES: ''
			}
		})
	});
	var fcgImage2 = new ol.layer.Image({
		title: '防城港点底图',
		source: new ol.source.ImageWMS({
			ratio: 1,
			url: 'http://120.241.64.28:9770/geoserver/cite/wms',
			params: {
				'FORMAT': 'image/png',
				'VERSION': '1.1.1',
				LAYERS: 'cite:point_new',
				STYLES: '',
			}
		})
	});
	var vector = new ol.layer.Vector({
		source: new ol.source.Vector({
			url: 'https://openlayers.org/en/v4.5.0/examples/data/geojson/countries.geojson',
			format: new ol.format.GeoJSON(),
			wrapX: false
		})
	});
	var map = new ol.Map({
		// 设置地图图层
		layers: [
			// 创建一个使用Open Street Map地图源的瓦片图层
			layer1, layer2, vector, fcgImage, fcgImage1, fcgImage2
		],
		// 设置显示地图的视图
		view: mapView,
		// 让id为map的div作为地图的容器
		logo: {
			src: '',
			href: ''
		}, // 点击能跳转到对应页面
		target: 'map',
		controls: ol.control.defaults({
			attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
				collapsible: false
			})
		}),
	});
	  map.addControl(zoomslider);

	//			map.on('click', function(evt) {
	//				//地图点击事件
	//		
	//			
	//				var coordinate = evt.coordinate;
	//	var hdms = ol.coordinate.toStringXY(ol.proj.transform(
	//		coordinate, 'EPSG:3857', 'EPSG:4326'),2);
	//	mui.alert(hdms);
	//			});

